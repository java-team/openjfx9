#!/bin/sh

VERSION=$2
TAR=../openjfx9_$VERSION.orig.tar.xz
TAG=jdk-$(echo $VERSION | sed -e 's/~b/+/g')
DIR=rt-$TAG

rm $3

wget -v http://hg.openjdk.java.net/openjfx/9u-dev/rt/archive/$TAG.tar.gz

tar -xf $TAG.tar.gz
rm $TAG.tar.gz

XZ_OPT=--best tar -c -v -J -f $TAR \
                  --exclude '.hg*' \
                  --exclude '.classpath' \
                  --exclude '.project' \
                  --exclude '.idea' \
                  --exclude 'apps/samples' \
                  --exclude 'modules/graphics/src/main/native-iio/libjpeg*' \
                  --exclude 'modules/media/src/main/native/gstreamer/3rd_party/glib/*' \
                  --exclude 'modules/web/src/main/native/Source/JavaScriptCore/inspector/scripts/jsmin.py' \
                  --exclude 'modules/web/src/main/native/Tools/TestResultServer/static-dashboards/dygraph-combined.js' \
                  $DIR

rm -Rf $DIR
